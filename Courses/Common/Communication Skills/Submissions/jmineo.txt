(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category): 0

Your categories:
2 Axure
6 Communication
0 Generic
8 HTML
1 Java
3 JIRA
4 OOP
5 SDLC
7 Unix Scripting

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses


For more help on adding questions, go to http://www.classmarker.com/a/help/#tests and look under the 'Import questions' section

(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): JAM
(Course Site):Udemy
(Course Name): Consulting Skills Series Communication
(Course URL):https://www.udemy.com/consulting-skills-series-communication/?dtcode=NXFwWSz1vMzW
(Discipline): Professional
(ENDIGNORE)

(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 0
(Question): When conducting meetings, it is important to create and publish an agenda in advance.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): It is important that all meeting attendees receive a copy of the agenda prior to the meeting so they can be prepared to answer any questions or provide input to the discussion.


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 0
(Question): What percentage of meeting attendees daydream during a meeting?
(A): 50%
(B): 3%
(C): 100%
(D): 93%
(E): 25%
(Correct): D
(Points): 1
(CF): 
(WF): On average 93% of all meeting attendees will daydream during the course of a meeting.


(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 0
(Question): When deciding who to invite to a meeting it best to invite as many people as possible. The more the better.
(A): True
(B): False
(Correct): B 
(Points): 1
(CF): 
(WF): It is important to invite only those who have to be there. Those who do not need to be there would find it a waste of time.


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 0
(Question): What is the attention span of the average adult who attends a meeting?
(A): 30 Seconds
(B): 1 Minute
(C): 1 Hour
(D): 8 hours
(E): 8 minutes
(Correct): D
(Points): 1
(CF): 
(WF): It has been found that adults typically have an attention span of 8 minutes.


(Type): multiplechoice
(Category): 6
(Grade style): 1
(Random answers): 0
(Question): What are the criteria for having an effective conference call? Choose all that apply.
(A): The call is necessary.
(B): There is a purpose to the call.
(C): An agenda has been created and distributed.
(D): As many people as possible have been invited.
(E): There is an expected result for the call.
(F): All of the above.
(Correct): A, B, C, E
(Points): 1
(CF): 
(WF): How to have an effective call: Ask “Do I need thecall?”, Is the time appropriate?, Is there a purpose for the call? What is the expected result? Have I created and distributed the agenda?, Is the attendee group as small as possible without inviting unnecessary people?, Do I know the environment and the equipment used for the call?


(Type): multiplechoice
(Category): 6
(Grade style): 1
(Random answers): 0
(Question): Ground rules for conference calls are important. What is NOT an example of conference call ground rules? Select all that apply.
(A): Don’t call from a cell phone.
(B): Turn your cell phone off.
(C): Keep your phone un-muted so you can respond as quickly as possible.
(D): Disable anything that makes noise (other devices, tablets, cell phones)
(E): Speak up.
(F): Identify yourself when speaking.
(Correct): C
(Points): 1
(CF): 
(WF): If you don’t have anything to add at the moment, it is a good idea to keep your phone muted to avoid creating any distracting background noise.


(Type): multiplechoice
(Category): 6
(Grade style): 0
(Random answers): 0
(Question): When is it appropriate to inject humor into a business email?
(A): Whenever it seems appropriate
(B): If there is an applicable Dilbert cartoon you can attach
(C): You should use humor sparingly and tactfully
(D): Never
(E): All the time. People love a good joke.
(Correct): D
(Points): 1
(CF): 
(WF): Humor is a subjective thing and can easily be misinterpreted. Avoid injecting humor in all business emails.


(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 0
(Question): True or False. It is OK to use “text speak” in your business emails. It is so common people accept it as a typical business practice.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): 
(WF): “Text speak” is a crude form of communication and has no place in business communications.


(Type): multiplechoice
(Category): 6
(Grade style): 1
(Random answers): 0
(Question): When structuring an email it is a good idea to___. Select all that apply.
(A): Be succinct and factual.
(B): Include politics and humor.
(C): Use BCC as often as possible to cover yourself.
(D): Don’t bother proof reading. Most email clients do a good job of catching spelling and grammatical errors.
(E): Send an email as quickly as possible when you are mad about something. That way the reason you are mad is fresh on your mind.
(F): All of the above.
(Correct): A
(Points): 1
(CF): 
(WF): Politics and humor are dangerous things to include in professional emails. Use BCC sparingly, it could come back to haunt you. Proof read all emails. Email clients often will not flag a properly spelled “wrong” word. NEVER send emails when you are mad. Use the draft folder, come back to the email when you have cooled off.


(Type): truefalse
(Category): 6
(Grade style): 0
(Random answers): 0
(Question): Powerpoint is the best way to communicate with executives. They love flashy presentations with lots of graphics.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): 
(WF): Most execs hate powerpoint because they suffer from "death by unplanned presentations", not planned on paper, not rehearsed.


