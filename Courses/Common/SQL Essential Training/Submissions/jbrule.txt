(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jon Brule
(Course Site): lynda.com
(Course Name): SQL Essential Training
(Course URL): http://www.lynda.com/SQL-tutorials/SQL-Essential-Training/139988-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): truefalse
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): Each table may have only one PRIMARY KEY. This key may include a single column, or it may
encompass multiple columns that together provide uniqueness for each row.
(A): True
(B): False
(Correct): A
(Points): 1
(CF):
(WF):
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): Within an After Insert trigger, one refers to the inserted record using the following alias
(A): CURRENT
(B): INSERTED
(C): NEW
(D): OLD
(Correct): C
(Points): 1
(CF): The 'NEW' alias provides access to the newly-inserted record, including any generated fields. (e.g. NEW.id).
(WF): The 'NEW' alias provides access to the newly-inserted record, including any generated fields. (e.g. NEW.id).
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): Which of the following joins are NOT typically supported in SQL?
(A): INNER JOIN
(B): LEFT OUTER JOIN
(C): RIGHT OUTER JOIN
(D): FULL OUTER JOIN
(Correct): C,D
(Points): 1
(CF): Generally, only INNER and LEFT OUTER JOINs are supported. Often, a RIGHT JOIN can be simulated by reversing the table relationship making it a LEFT JOIN.
(WF): Generally, only INNER and LEFT OUTER JOINs are supported. Often, a RIGHT JOIN can be simulated by reversing the table relationship making it a LEFT JOIN.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
