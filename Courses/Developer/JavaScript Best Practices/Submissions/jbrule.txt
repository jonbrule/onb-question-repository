(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jon Brule
(Course Site): Code School
(Course Name): JavaScript Best Practices
(Course URL): https://www.codeschool.com/courses/javascript-best-practices
(Discipline): Technical
(ENDIGNORE)


(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Falsy values in JavaScript are [code]false[/code], and [code]0[/code]. All other values are truthy. 
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Falsy values: [code]false[/code], [code]0[/code], [code]undefined[/code], [code]NaN[/code], [code]""[/code], and [code]null[/code]. All other values are truthy.
(WF): Falsy values: [code]false[/code], [code]0[/code], [code]undefined[/code], [code]NaN[/code], [code]""[/code], and [code]null[/code]. All other values are truthy. 
(STARTIGNORE)
(Hint): Think of the "non-data" cases.
(Subject): Ternary Conditionals
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Which of the following statements is MOST true?
(A): Ternaries cannot be nested.
(B): Ternaries can be nested.
(C): Ternaries can be nested, but don't do this too much due to its inherent lack of clarity.
(D): Ternaries are special JavaScript background processes that increase performance.
(Correct): C
(Points): 1
(CF): Ternaries can be nested, but don't do this too much due to its inherent lack of clarity.
(WF): Ternaries can be nested, but don't do this too much due to its inherent lack of clarity.
(STARTIGNORE)
(Hint): Not a best practice when deeply used.
(Subject): Ternary Conditionals
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): When using a logical OR (||) assignment, if all elements are "falsy", then the return value will be the LAST "falsy" value. 
(A): True
(B): False
(Correct): A
(Points): 1
(CF): When using a logical OR (||) assignment, if all elements are "falsy", then the return value will be the LAST "falsy" value.
(WF): When using a logical OR (||) assignment, if all elements are "falsy", then the return value will be the LAST "falsy" value. 
(STARTIGNORE)
(Hint): No one is in a rush to be first...
(Subject): Logical Assignment
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): When using a logical AND (&&) assignment, if all elements are "falsy", then the return value will be the LAST "falsy" value. 
(A): True
(B): False
(Correct): B
(Points): 1
(CF): When using a logical AND (&&) assignment, if all elements are "falsy", then the return value will be the FIRST "falsy" value.
(WF): When using a logical AND (&&) assignment, if all elements are "falsy", then the return value will be the FIRST "falsy" value. 
(STARTIGNORE)
(Hint): Don't get in my way...
(Subject): Logical Assignment
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What purpose does [code]break[/code] statement serve in a [code]switch[/code] statement?
(A): It allows a case to immediately exit the switch without falling through to subsequent cases
(B): It exits the active program
(C): It causes a particualr case to halt, forwarding to the next case within the switch
(D): It pauses execution so that you can evaluate variables in a debugger
(Correct): A
(Points): 1
(CF): It allows a case to immediately exit the switch without falling through to subsequent cases.
(WF): It allows a case to immediately exit the switch without falling through to subsequent cases.
(STARTIGNORE)
(Hint): Halts any fall-through execution
(Subject): Switch Block
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): It is not possible to mix types within a switch statement. 
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Switch statements can mix different types for its cases.
(WF): Switch statements can mix different types for its cases. 
(STARTIGNORE)
(Hint):
(Subject): Switch Block
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): For a given array [code]list[/code], which of the following is the best way to iterate over the array's itmes?
(A): [code]for(var i=0, x=list.length; i < x; i++) …[/code]
(B): [code]for(var i=0; i < list.length; i++) …[/code]
(C): [code]for(var item in list) …[/code]
(Correct): A
(Points): 1
(CF): [code]for(var i=0, x=list.length; i < x; i++) …[/code] <-- Eliminates calls into [code]list[/code] to determine its length with each iteration.
(WF): [code]for(var i=0, x=list.length; i < x; i++) …[/code] <-- Eliminates calls into [code]list[/code] to determine its length with each iteration.
(STARTIGNORE)
(Hint): Caching provides the better performance
(Subject): Loop Optimization
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Any  work-intensive script in the [code]head[/code] or higher in the [code]body[/body] can have an impact on page loading. 
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Any large script in the [code]head[/code] or higher in the [code]body[/body] can have an impact on page loading.
(WF): Any large script in the [code]head[/code] or higher in the [code]body[/body] can have an impact on page loading. 
(STARTIGNORE)
(Hint):
(Subject): Script Execution
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What two optimization techniques can be used to speed up slow pages due to script loading?
(A): Relocate work-intensive scripts to bottom of the [code]body[/code] tag
(B): Add HTML5 [code]async[/code] attribute to [code]script[/code] tag
(C): Refresh the page to reload the cache
(D): Relocate work-intensive scripts to top of the [code]body[/code] tag
(E): Add HTML5 [code]cached[/code] attribute to [code]script[/code] tag
(Correct): A, B
(Points): 1
(CF): Move scripts to bottom of [code]body[/code], or use the [code]async[/code] attribute within a head script.
(WF): Move scripts to bottom of [code]body[/code], or use the [code]async[/code] attribute within a head script.
(STARTIGNORE)
(Hint): 
(Subject): Script Execution
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is wrong with the following module inport of a global variable?
[code]
var wartime = true;
var ARMORY = (function(war){
    var weaponList = [ *list of weapon Objects* ];
    
    var removeWeapon = function(...){};
    var replaceWeapon = function(...){};
    
    return {
        makeWeaponRequest: function(...){
            if (wartime) // let civilians have weaponry
        }
    };	
})(wartime);
[/code]
(A): The [code]wartime[/code] global variable is mapped to the local [code]war[/code] variable; however, the local variable is not used.
(B): There is no need to pass the [code]wartime[/code] variable as it is globally-accessible.
(C): Global imports are an anti-pattern.
(D): Previous private data will NOT be accessible to the new properties.
(Correct): A
(Points): 1
(CF): The [code]wartime[/code] global variable is mapped to the local [code]war[/code] variable; however, the local variable is not used.
(WF): The [code]wartime[/code] global variable is mapped to the local [code]war[/code] variable; however, the local variable is not used.
(STARTIGNORE)
(Hint): 
(Subject): Script Execution
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)