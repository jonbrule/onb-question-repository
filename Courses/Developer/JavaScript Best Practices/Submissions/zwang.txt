(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Ziyan Wang
(Course Site): Code School
(Course Name): JavaScript Best Practices
(Course URL): https://www.codeschool.com/courses/javascript-best-practices/
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): Which are good practices for Javascript loop optimization? (choose three)
(A): Limit memory access
(B): Avoid repetitive access at depth
(C): Place control variables inside the loop
(D): Declare many global variables inside the loop
(Correct): A,B,C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Javascript
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What does the join() method do in Javascript?
(A): Joins the elements of an array into a string, and returns the string.
(B): Joins the values of an object into a string, and returns the string.
(C): Joins two arrays into one array
(D): Joins two strings into one string
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Javascript
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): When do we need to use instanceof in Javascript?
(A): To check if an object in its prototype chain has the prototype property of a constructor.
(B): To check if an object inherits from Object.prototype
(C): To check if an object is a singleton
(D): To check if an object is created using object literal
(Correct): A
(Points): 1
(CF): instanceof is used to check if an object in its prototype chain has the prototype property of a constructor.
(WF): instanceof is used to check if an object in its prototype chain has the prototype property of a constructor.
(STARTIGNORE)
(Hint):
(Subject): Javascript
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): It is not a good practice to leave off the '{}' in conditional statements.
(A): True
(B): False
(Correct): A 
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Javascript
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): In Javascript, what is the cause for 0.1 + 0.2 = 0.30000000000004 ?  
(A): Javascript has dynamic types
(B): Javascript arithmetic operations are not stable
(C): Javascript features functional programming
(D): Javascript uses binary floating point values
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Javascript
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is the result type after using toFixed() method?
(A): string
(B): number
(C): boolean
(D): object
(Correct): A
(Points): 1
(CF): String is the result type after using toFixed() method
(WF): String is the result type after using toFixed() method
(STARTIGNORE)
(Hint):
(Subject): Javascript
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is the output of NaN === NaN?
(A): True
(B): False
(C): Undefined
(D): Syntax error
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Javascript
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What can be done to avoid polluting the global namespace?
(A): Declare variables as properties within an object
(B): Declare variables with unique names
(C): Declare variables without the keyword 'var'
(D): Declare variables as array elements
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Javascript
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): How to implement private variables in Javascript?
(A): use the keyword private
(B): use closures
(C): use self-invoking function
(D): use anonymous function
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Javascript
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 10
(Grade style): 0
(Random answers): 1
(Question): What is the problem when many non-local variables are referenced in a function? (choose two)
(A): The entire scope chain is checked
(B): Poor readability
(C): Functions may have unexpected return values
(D): Javascript may throw exceptions
(Correct): A,B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Javascript
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
