(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jon Brule
(Course Site): Udemy
(Course Name): Productive Coding with WebStorm
(Course URL): https://www.udemy.com/productive-coding-with-webstorm/learn/v4/overview
(Discipline): Technical
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What is the WebStorm EAP?
(A): A facility allows download of pre-released versions of WebStorm.
(B): A program that allows you to access your code off-hours.
(C): A tool that helps to architect your applications.
(D): A component that builds your applications. 
(Correct): A
(Points): 1
(CF): WebStorm EAP allows early access to pre-released versions of WebStorm
(WF): WebStorm EAP allows early access to pre-released versions of WebStorm
(STARTIGNORE)
(Hint): EAP stands for Early Access Program
(Subject): Installation
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): WebStorm is available for the following platforms (choose all that apply):
(A): Mac OSX
(B): Windows
(C): Linux
(D): VMS
(E): MSDOS
(F): ChromeOS
(G): Cloud
(Correct): A, B, C
(Points): 1
(CF): WebStorm is available for the Mac OSX, Windows, and Linux platforms. 
(WF): WebStorm is available for the Mac OSX, Windows, and Linux platforms. 
(STARTIGNORE)
(Hint):
(Subject): Installation
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): On Windows, [code]Alt+Ins[/code] is the shortcut for creating a new file.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): On Windows, [code]Alt+Ins[/code] is the shortcut for creating a new file. 
(WF): On Windows, [code]Alt+Ins[/code] is the shortcut for creating a new file. 
(STARTIGNORE)
(Hint):
(Subject): Touring the User Interface
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Which mode allows you to drag panels to other monitors?
(A): Pinned Mode
(B): Float Mode
(C): Docked Mode
(Correct): B
(Points): 1
(CF): Enabling Float Mode allows you to drag panes to other monitors for extra room. 
(WF): Enabling Float Mode allows you to drag panes to other monitors for extra room. 
(STARTIGNORE)
(Hint):
(Subject): Adapting the User Interface
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): You can pin panels on all four sides of the screen.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): You can pin panels on all four sides of the screen. 
(WF): You can pin panels on all four sides of the screen.
(STARTIGNORE)
(Hint):
(Subject): Installation
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Settings can only be exported in full but imported in part.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Settings can only be exported in full but imported in part. 
(WF): Settings can only be exported in full but imported in part. 
(STARTIGNORE)
(Hint):
(Subject): Installation
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): The [code]div*3>lorem[/code] Emmet abbreviation expands to which of the following in an HTML file?
(A): The first three words of the "Lorem ipsum" text
(B): The first three sentences of the "Lorum ipsum" text, each in its own [code]p[/code] block.
(C): Random excerpts from the "Lorem ipsum" text across three [code]div[/code] blocks.
(Correct): C
(Points): 1
(CF): Random excerpts from the "Lorem ipsum" text across three [code]div[/code] blocks 
(WF): Random excerpts from the "Lorem ipsum" text across three [code]div[/code] blocks 
(STARTIGNORE)
(Hint): [code]lorem3[/code] generates the first three words of the "Lorem ipsum" text
(Subject): Using Emmet for HTML
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Which of the following Emmet abbreviations expands to an unordered list with three items, each of which has the "item #" text where # is replaced with the relative line number?
(A): [code]ul>li*3>a{item $}[/code]
(B): [code]ul>li{item $}*3[/code]
(C): [code]ul>li*3{item $}[/code]
(D): [code]ul>li+{item $}3[/code]
(Correct): B
(Points): 1
(CF): [code]ul>li{item $}*3[/code] generates an unordered list with three items, each containing "item #" text where # is incremented from 1
(WF): [code]ul>li{item $}*3[/code] generates an unordered list with three items, each containing "item #" text where # is incremented from 1 
(STARTIGNORE)
(Hint): [code]lorem3[/code] generates the first three words of the "Lorem ipsum" text
(Subject): Using Emmet for HTML
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Pressing the [code]Shift[/code] key twice will open the "Search Anywhere" functionality.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Pressing the [code]Shift[/code] key twice will open the "Search Anywhere" functionality. 
(WF): Pressing the [code]Shift[/code] key twice will open the "Search Anywhere" functionality. 
(STARTIGNORE)
(Hint):
(Subject): Quick Navigation to files, folder, and code
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Live Template variables take the form of [code]${VAR}[/code], where VAR is the variable name.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Live Template variables take the form of [code]$VAR$[/code].
(WF): Live Template variables take the form of [code]$VAR$[/code]. 
(STARTIGNORE)
(Hint): File Templates, which are based upon Apache Velocity, use the [code]${VAR}[/code] format.
(Subject): Creating Live Templates
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)