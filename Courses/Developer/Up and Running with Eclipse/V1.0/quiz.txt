(Type): multiplechoice
(Category): 22
(Question): What is the easiest way to generate getters and setters in Eclipse?
(A): Copy/paste 
(B): Create them by hand
(C): Have a data entry person type them for you
(D): "Right Cl > Source > Generate Getters and Setters"
(Correct): D
(CF): The easiest way to generate getters and setters in Eclipse is to "Right Cl > Source > Generate Getters and Setters".
(WF): The easiest way to generate getters and setters in Eclipse is to "Right Cl > Source > Generate Getters and Setters".


(Type): multiplechoice
(Category): 22
(Question): What view in Eclipse is similar to that of Windows Explorer?
(A): Outline
(B): Navigator
(C): Junit View
(D): Package Explorer
(Correct): B
(CF): The navigator view in Eclipse is very similar to Windows Explorer.
(WF): The navigator view in Eclipse is very similar to Windows Explorer.


(Type): multiplechoice
(Category): 22
(Question): If you want to try and stop at a certain point in your code what steps would you take in Eclipse?
(A): Set a breakpoint by clicking on the left gutter and "Run As a Junit test"
(B): Click on the same line in the left gutter to set a breakpoint, simply select "Run As a Java Application" and it will stop
(C): Click on the same line in the right gutter to set a breakpoint, simply select "Run As a Java Application" and it will stop
(D): Click on the same line in the left gutter to set a breakpoint, when you go to run the application select "Debug As A Java Application"
(Correct): D
(CF): To stop at a breakpoint in Eclipse you need to click on the same line in the left gutter to set the breakpoint.  After that you need to execute the code using "Debug As a Java Application".
(WF): To stop at a breakpoint in Eclipse you need to click on the same line in the left gutter to set the breakpoint.  After that you need to execute the code using "Debug As a Java Application".


(Type): multiplechoice
(Category): 22
(Question): How does one easily resolve missing imports when using classes from a new package? (Select all that apply)
(A): Click "File > Resolve Missing Imports"
(B): Click "Ctrl-Sh-O" to automatically import
(C): Left click the gutter that show the problem and select the import needed
(D): Hover over the item that is marked in red and click the import that is desired
(E): Go out on the web to determine the package that needs to be imported and paste it at the top of your class
(Correct): B,C,D
(CF): There are multiple ways to resolve imports in eclipse here are the options; "Ctrl-Sh-O", left click the gutter where the problem is and select your desired import, or hover over the class that is marked in red and select the desired import.
(WF): There are multiple ways to resolve imports in eclipse here are the options; "Ctrl-Sh-O", left click the gutter where the problem is and select your desired import, or hover over the class that is marked in red and select the desired import.


(Type): multiplechoice
(Category): 22
(Question): What is an IDE?
(A): A basic editor
(B): A utility to connect to databases
(C): An application that helps with FTPing
(D): An environment that facilitates developers in building software
(Correct): D
(CF): An IDE is an integrated development environment that helps developers build software.
(WF): An IDE is an integrated development environment that helps developers build software.


(Type): multiplechoice
(Category): 22
(Question): What is the preferred method of renaming a variable using Eclipse?
(A): Search/replace
(B): Deleting the class and starting over
(C): Using "Refactor > Rename" to rename the variable
(D): Manually searching and pasting the new variable name wherever the old one is found
(Correct): C
(CF): The fastest way to rename all instances of a variable within a class in eclipse is to use the "Refactor > Rename" function.
(WF): The fastest way to rename all instances of a variable within a class in eclipse is to use the "Refactor > Rename" function.


(Type): multiplechoice
(Category): 22
(Question): What is the term used for a saved view setup within Eclipse?
(A): A view
(B): A perspective
(C): The window setup
(D): The window formatting
(E): The view configuration
(Correct): B
(CF): The term used for a saved window setup within Eclipse is a perspective.
(WF): The term used for a saved window setup within Eclipse is a perspective.


(Type): multiplechoice
(Category): 22
(Question): What is meant when one refers to the gutter in Eclipse?
(A): A source of garbage collection
(B): Any code that doesn't compile goes here
(C): It is where you drag things to be deleted
(D): It's the column that icons appear just to the left of the code
(Correct): D
(CF): The gutter is the column that icons appear in just to the left of the code.
(WF): The gutter is the column that icons appear in just to the left of the code.


(Type): multiplechoice
(Category): 22
(Question): What function does "Step Over" perform when debugging?
(A): It runs the rest of the scripts code
(B): It skips the function or block of code
(C): Nothing unless it comes across a class
(D): It performs the function or block of code
(E): It moves line by line through the function or block of code
(Correct): E
(CF): The "Step Over" function walks through the code performing each line one by one as it is repeatedly called.
(WF): The "Step Over" function walks through the code performing each line one by one as it is repeatedly called.


(Type): multiplechoice
(Category): 22
(Question): How can you search your entire workspace for a specific string? 
(A): "Ctrl-F" and type in the string to search
(B): Click "File > Search Workspace" at the top menubar
(C): Click "Search > File" and type in the search string
(D): Exit Eclipse and use a tool like Notepad ++ to search within files
(Correct): C
(CF): You can search an entire project by clicking "Search > File" and type in the search string.
(WF): You can search an entire project by clicking "Search > File" and type in the search string.


(Type): multiplechoice
(Category): 22
(Question): If you do not like a view in Eclipse how do you get rid of it? 
(A): Double click the view
(B): Click and hold the view and drag it into the gutter
(C): Click the x icon in the top right corner of the view
(D): Click and hold the view while dragging to lower right hand corner of the screen.
(Correct): C
(CF): You can close a view by simply clicking the x icon in the upper right hand corner.
(WF): You can close a view by simply clicking the x icon in the upper right hand corner.


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What is an Eclipse plugin used for?
(A): Eclipse plugins are used for debugging only
(B): A plugin is a way to plug java code into the IDE
(C): An Eclipse plugin is a perspective that can be viewed
(D): It is used for extending the functionality of the IDE 
(Correct): D
(CF): An Eclipse plugin is used to extend the functionality of the IDE.
(WF): An Eclipse plugin is used to extend the functionality of the IDE.


(Type): multiplechoice
(Category): 22
(Question): What is the best way to install an Eclipse plugin?
(A): Click "File > Add Plugin" from the top menu bar
(B): Click "Help > Install New Software" from the top menu bar
(C): Manually download it and copy it to the workspace directory
(D): Use the Eclipse lazy loader utility to download and install it for you
(Correct): C
(CF): The Eclipse plugin manager is the easiest way to get and install a new plugin.  You can use this by clicking "Help > Install New Software" from the top menu bar.
(WF): The Eclipse plugin manager is the easiest way to get and install a new plugin.  You can use this by clicking "Help > Install New Software" from the top menu bar.


(Type): multiplechoice
(Category): 22
(Question): Where do system.out and system.err get output when running in Eclipse,?
(A): They are output to the console view
(B): They get output to a command prompt window 
(C): They will be output to the output window in Eclipse
(D): They go into a special log file that Eclipse will create in your workspace
(Correct): A
(CF): The system.out and system.err outputs will get logged to the console view in Eclipse.
(WF): The system.out and system.err outputs will get logged to the console view in Eclipse.


(Type): multiplechoice
(Category): 22
(Question): What version of java does Eclipse support?
(A): 1.6
(B): 1.5
(C): 1.5 and up
(D): All versions that you have installed on the machine
(Correct): D
(CF): Eclipse can support any version of java that you have installed on your working machine.  You simply need to tell Eclipse where the installation exists.
(WF): Eclipse can support any version of java that you have installed on your working machine.  You simply need to tell Eclipse where the installation exists.


(Type): multiplechoice
(Category): 22
(Question): Which three choices below are things that you can do with the Eclipse debug functionality?(Choose three)
(A): Set breakpoints 
(B): Watch variables
(C): View objects and their data
(D): Step through the code being executed
(E): Modify the execution timing of a thread
(Correct): A,B,C,D
(CF): You can do many things with the Eclipse debugger such as; setting breakpoints, watching variables, viewing objects, and stepping through the code.
(WF): You can do many things with the Eclipse debugger such as; setting breakpoints, watching variables, viewing objects, and stepping through the code.


(Type): multiplechoice
(Category): 22
(Question): How do you add program arguments when running your application in Eclipse?
(A): You can enter them in the console
(B): Click "File > Add Program Arguments" from the top menu bar
(C): Go to the Debugging view, select the arguments tab and enter them there
(D): Go to "Run Configurations > Arguments Tab" and enter in the program arguments section
(Correct): D
(CF): Go to "Run Configurations > Arguments Tab", and enter in the program arguments section.
(WF): Go to "Run Configurations > Arguments Tab", and enter in the program arguments section.


(Type): multiplechoice
(Category): 22
(Question): How would you quickly navigate into a method while viewing code in eclipse?
(A): Left click the mouse over the method
(B): Select "Alt-Tab" to jump into the method
(C): Press "ctrl and left click" on the method
(D): Click "File > Go To Method" in the top menu bar
(Correct): C
(CF): The fastest way to navigate into a method in eclipse is to press the ctrl button and left click.
(WF): The fastest way to navigate into a method in eclipse is to press the ctrl button and left click.


(Type): multiplechoice
(Category): 22
(Question): What is an easy way to view all of the classes that call a particular method?
(A): Select the method and press "ctrl-shift-g" 
(B): Exit eclipse and search for text within a file in windows
(C): Click "Search > File" to search for text within all files in eclipse
(D): Select the method and press "ctrl-alt-tab" and left click on the method
(Correct): A
(CF): The easiest way to view all of the classes that call a particular method is pressing "ctrl-shift-g".
(WF): The easiest way to view all of the classes that call a particular method is pressing "ctrl-shift-g".


(Type): multiplechoice
(Category): 22
(Question): If you want to autocomplete within eclipse to see what methods are available what keystroke to you press?
(A): Press "Ctrl-Tab" 
(B): Press "Ctrl-Space" 
(C): Press "Shift" while left clicking the mouse 
(D): Press "Alt-Shift" while left clicking the mouse
(Correct): B
(CF): In order to invoke the autocomplete feature in eclipse you must press "Ctrl-Space".
(WF): In order to invoke the autocomplete feature in eclipse you must press "Ctrl-Space".