(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Megan Barton
(Course Site): Lynda
(Course Name): Mastering Selenium Testing Tools
(Course URL): https://www.lynda.com/Selenium-tutorials/Mastering-Selenium-Testing-Tools/521207-2.html
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Selenium allows for ________ in various browsers.
(A): web application creation
(B): web application testing
(C): java and IDEA integration
(D): java platform coding 
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Selenium
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 2
(Random answers): 0
(Question): Which programming language(s) can you use with Selenium to control a browser?
(A): C#
(B): Java
(C): Javascript
(D): All of the above
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Selenium
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): True or False: You can test a web application remotely using WebDriver or locally using Selenium Server.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Selenium
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which one of the following is not true of assertion and verification?
(A): Assertion is fail fast and verification is fail slow.
(B): If a verification fails the script can continue.
(C): If a verification fails the script will immediately stop.
(D): If an assertion fails the script will immediately stop.
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Selenium
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): What are some of the advantages of using WebDriver to test web applications?
(A): You can test on various operating systems and hardware platforms
(B): All of these are advantages of using WebDriver to test web applications
(C): You can use various programming languages to control automation
(D): You can use various web browsers and it is not limited to Firefox
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Selenium
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Cross-browser testing is best described as testing web applications on _____.
(A): different operating systems
(B): headless browsers such as HtmlUnit
(C): a variety of hardware platforms
(D): a variety of browsers
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Selenium
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which WebDriver method would you use to navigate to a desired URL?
(A): getURL()
(B): getTitle()
(C): get()
(D): getCurrentURL()
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Selenium
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category):18
(Grade style): 0
(Random answers): 1
(Question): What is one of the fastest and most dependable locators you can use to search for elements in a web application?
(A): ByClassName
(B): ByLinkText
(C): ByCssSelector
(D): ById
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Selenium
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): True or False: The WebElement interface provides methods to interact with the elements of a page.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Selenium
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): True or False: A page object is a complete copy of a page from a web application.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Selenium
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)